import React, { Component } from 'react';
import Question from './Question';

const questions = [
  {
    id: 1,
    question: 'Where does it come from?',
    answer: 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
  },
  {
    id: 2,
    question: 'Why do we use it?',
    answer: 't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  },
]

class App extends Component {
  render() {
    return (
      <div>
        {questions.map(question => (
          <Question
            key={question.id}
            question={question.question}
          >
            {question.answer}
          </Question>
        ))}
      </div>
    );
  }
}

export default App;
